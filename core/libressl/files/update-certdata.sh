#!/bin/sh -e

if [ -w /etc/ssl/cacert.pem ]; then
    cd /etc/ssl && {
        curl -LO https://curl.haxx.se/ca/cacert.pem
        mv -f cacert.pem cert.pem
        printf '%s\n' "${0##*/}: updated cert.pem"
    }
else
    printf "Skipping updating certdata as /etc/ssl/cacert.pem is not writable\n"
fi
